(function($){
    $(document).ready(function(){
        $('.tab-container').each(function() {
			var ul = $(this).find('ul.etabs');

			$(this).children('div.tabs').each(function(e) {
				var widget = $(this).attr('id');
				$(this).find('a.tab-title').attr('href', '#' + widget).wrapInner('<span></span>').wrap('<li class="tab"></li>').parent().detach().appendTo(ul);

                $(this).css({
                    'visibility': 'visible'
                });
			});

			$(this).easytabs({
				animate: true,
				updateHash: false,
			});

		});

        $('.prettySocial').prettySocial();
    });

    var resizeMasonry = function() {
        $('.masonry-widget > .item').each(function(){
            var $item = $(this);
            if ( $item.hasClass('size11') || $item.hasClass('size-22') ) {
                $item.css('height', $item.width());
            } else if ( $item.hasClass('size21') ) {
                $item.css('height', $item.width()/2);
            } else if ( $item.hasClass('size-12') ) {
                $item.css('height', $item.width()*2);
            }
        });
    };

    $(window).resize(resizeMasonry);
    resizeMasonry();

    $(window).load(function(){
        var $container = $('.masonry-widget');
        $container.packery({
            itemSelector: '.item',
            gutter: 0
        });
        /* $container.nested({
            selector: '.item',
            minWidth: 265,
            gutter: 30
        }); */

    });
})(jQuery);
